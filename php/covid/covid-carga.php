<?php
	header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');

	//NOMBRE DE ARCHIVO
	$fileList = glob("../../assets/covid/*.pdf");
	//ORDENAMOS EL ARREGLO DE ARCHIVOS POR FOLIO
	natsort($fileList);
	//$fileList = array_reverse($fileList, false);
	//RECORREMOS LOS ARCHIVOS n VECES
    $n = 0;
	foreach($fileList as $filename) {
	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {
           $resultados[] = array("success"=> true, "nombre"=> utf8_encode(str_replace(".pdf","" , str_replace("../../assets/covid/", "", $filename))), "url"=> str_replace("../../", "", $filename));
		} else {
            $resultados[] = array("success"=> false, "message"=> "Error de archivo " . error_get_last() );
		}
	}
    print json_encode($resultados);
?>
