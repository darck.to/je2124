<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-type: application/json');
    include_once('../../backend/functions/abre_conexion.php');
    
    // Inicializacmos la variable resultado
    $resultados = array();

    // Leemos los archivos de la carpeta de notas
    $notas = glob('../../assets/news/*_boletin_*.json');
    // Recorremos todo el arregle de notas
    foreach ($notas as $contenido) {
        // Revisamos si es un archivo
        if (file_exists($contenido)) {
            // Read the JSON file 
            $json = file_get_contents($contenido);
            // Decode the JSON file
            $data = json_decode($json,true);
            // Leemos data
            foreach ($data as $value) {
                $titulo = $value["title"];
                $nota = $value["note"];
                $folio = $value["folio"] . "_boletin_" . $value["random"] . "_" . $value["date"];
            }
            if ($sqlDire = $mysqli->query("INSERT INTO `notas_table`(`titulo`, `cuerpo`, `folio`) VALUES ('$titulo', '$nota', '$folio')")) {
                $resultados[] = array('success'=>true, "message"=> "Insertado folio: " . $folio);    
            } else {
                $resultados[] = array("success"=> false, "error"=> mysqli_error($mysqli));
            }
            
        }
    }
    
    include('../../backend/functions/cierra_conexion.php');
    print json_encode($resultados);
?>
