<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-type: application/json');
    include_once('../../backend/functions/abre_conexion.php');

    // Fecha actual
    date_default_timezone_set("America/Mexico_City");
    $fechaActual = Date('d-m-Y H:i:s');
    // Inicializacmos la variable resultado
    $resultados = array();

    //"limpiamos" los campos del formulario de posibles códigos maliciosos
    $busqueda = htmlspecialchars($_POST['valor']);

    $sql = $mysqli->query("SELECT `titulo`, SUBSTRING_INDEX(`cuerpo`,' ',12) AS cuerpo, `folio` FROM `notas_table` WHERE `titulo` LIKE '%$busqueda%' OR `cuerpo` LIKE '%$busqueda%' ORDER BY `folio` DESC LIMIT 20");
        if ($sql->num_rows > 0) {
            while ($row = $sql->fetch_assoc()) {
                $resultados[] = array("success"=> true, "titulo"=>$row["titulo"], "cuerpo"=>$row["cuerpo"], "folio"=>$row["folio"]);
            }
        } else {
            $resultados[] = array("success"=> false, "message"=> "No Resultados");
        }

    include('../../backend/functions/cierra_conexion.php');
    print json_encode($resultados);
?>
