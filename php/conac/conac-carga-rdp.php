<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/functions.php');
  //NOMBRE DE ARCHIVO
	$fileList = glob("../../assets/rdp/*.pdf");
	//ORDENAMOS EL ARREGLO DE ARCHIVOS POR FOLIO
	natsort($fileList);
	$fileList = array_reverse($fileList, false);
	foreach($fileList as $filename) {
   	//SI SOY ARCHIVOS PDF LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {
		  $resultados[] = array("success"=> true, "file"=>utf8_encode(str_replace(".pdf","" , str_replace("../../assets/rdp/", "", $filename))), "link"=>str_replace("../../", "", $filename));
		}
	}
  print json_encode($resultados);
?>
