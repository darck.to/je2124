<?php
	header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');

  include_once('../../functions/functions.php');

  $dir = "../../assets/conac/";
	$exclude = array( ".","..","error_log","_notes","meta_file.json","desktop.ini" );
	if (is_dir($dir)) {
		$files = scandir($dir);
		foreach($files as $dir){
			if(!in_array($dir,$exclude)) {
                $resultados[] = array("success"=> true, "tdd"=> str_replace("_", " ", $dir), "td1"=> "", "td2"=> "", "td3"=> "", "td4"=> "", "td5"=> "", "td6"=> "", "td7"=> "", "td8"=> "", "td9"=> "", "td10"=> "", "td11"=> "", "td12"=> "", "td13"=> "", "td14"=> "", "td15"=> "", "td16"=> "", "td17"=> "", "td18"=> "", "td19"=> "", "td20"=> "", "td21"=> "", "td22"=> "", "td23"=> "");
				$subdir = "../../assets/conac/" . $dir . "/";
				if (is_dir($subdir)) {
					$arraysubdir = scandir($subdir);
					array_multisort($arraysubdir,SORT_NUMERIC, SORT_ASC);
					foreach($arraysubdir as $current_dir){
						if(!in_array($current_dir,$exclude)){
							// Array con condiciones para las subcarpetas
							$dirs = array("01_23","02_23","03_23","04_23","01_22","02_22","03_22","04_22","01_21","02_21","03_21","04_21","01_20","02_20","03_20","04_20","01_19","02_19","03_19","04_19","01_18","02_18","03_18","04_18");
							// Leémos de acuerdo al numero de carpetas y archivos en each dir & currentdir
                            $file0 = "assets/conac/" . $dir . "/" . $current_dir . "/01_23/file.pdf";
                            if (curlExist("../../" . $file0)) {
                                $td0 = $file0;
                            } else {
                                $td0 = false;
                            }
                            $file1 = "assets/conac/" . $dir . "/" . $current_dir . "/02_23/file.pdf";
                            if (curlExist("../../" . $file1)) {
                                $td1 = $file1;
                            } else {
                                $td1 = false;
                            }
                            $file2 = "assets/conac/" . $dir . "/" . $current_dir . "/03_23/file.pdf";
                            if (curlExist("../../" . $file2)) {
                                $td2 = $file2;
                            } else {
                                $td2 = false;
                            }
                            $file3 = "assets/conac/" . $dir . "/" . $current_dir . "/04_23/file.pdf";
                            if (curlExist("../../" . $file3)) {
                                $td3 = $file3;
                            } else {
                                $td3 = false;
                            }
                            $file4 = "assets/conac/" . $dir . "/" . $current_dir . "/01_22/file.pdf";
                            if (curlExist("../../" . $file4)) {
                                $td4 = $file4;
                            } else {
                                $td4 = false;
                            }
                            $file5 = "assets/conac/" . $dir . "/" . $current_dir . "/02_22/file.pdf";
                            if (curlExist("../../" . $file5)) {
                                $td5 = $file5;
                            } else {
                                $td5 = false;
                            }
                            $file6 = "assets/conac/" . $dir . "/" . $current_dir . "/03_22/file.pdf";
                            if (curlExist("../../" . $file6)) {
                                $td6 = $file6;
                            } else {
                                $td6 = false;
                            }
                            $file7 = "assets/conac/" . $dir . "/" . $current_dir . "/04_22/file.pdf";
                            if (curlExist("../../" . $file7)) {
                                $td7 = $file7;
                            } else {
                                $td7 = false;
                            }
                            $file8 = "assets/conac/" . $dir . "/" . $current_dir . "/01_21/file.pdf";
                            if (curlExist("../../" . $file8)) {
                                $td8 = $file8;
                            } else {
                                $td8 = false;
                            }
                            $file9 = "assets/conac/" . $dir . "/" . $current_dir . "/02_21/file.pdf";
                            if (curlExist("../../" . $file9)) {
                                $td9 = $file9;
                            } else {
                                $td9 = false;
                            }
                            $file10 = "assets/conac/" . $dir . "/" . $current_dir . "/03_21/file.pdf";
                            if (curlExist("../../" . $file10)) {
                                $td10 = $file10;
                            } else {
                                $td10 = false;
                            }
                            $file11 = "assets/conac/" . $dir . "/" . $current_dir . "/04_21/file.pdf";
                            if (curlExist("../../" . $file11)) {
                                $td11 = $file11;
                            } else {
                                $td11 = false;
                            }
                            $file12 = "assets/conac/" . $dir . "/" . $current_dir . "/01_20/file.pdf";
                            if (curlExist("../../" . $file12)) {
                                $td12 = $file12;
                            } else {
                                $td12 = false;
                            }
                            $file13 = "assets/conac/" . $dir . "/" . $current_dir . "/02_20/file.pdf";
                            if (curlExist("../../" . $file13)) {
                                $td13 = $file13;
                            } else {
                                $td13 = false;
                            }
                            $file14 = "assets/conac/" . $dir . "/" . $current_dir . "/03_20/file.pdf";
                            if (curlExist("../../" . $file14)) {
                                $td14 = $file14;
                            } else {
                                $td14 = false;
                            }
                            $file15 = "assets/conac/" . $dir . "/" . $current_dir . "/04_20/file.pdf";
                            if (curlExist("../../" . $file15)) {
                                $td15 = $file15;
                            } else {
                                $td15 = false;
                            }
                            $file16 = "assets/conac/" . $dir . "/" . $current_dir . "/01_19/file.pdf";
                            if (curlExist("../../" . $file16)) {
                                $td16 = $file16;
                            } else {
                                $td16 = false;
                            }
                            $file17 = "assets/conac/" . $dir . "/" . $current_dir . "/02_19/file.pdf";
                            if (curlExist("../../" . $file17)) {
                                $td17 = $file17;
                            } else {
                                $td17 = false;
                            }
                            $file18 = "assets/conac/" . $dir . "/" . $current_dir . "/03_19/file.pdf";
                            if (curlExist("../../" . $file18)) {
                                $td18 = $file18;
                            } else {
                                $td18 = false;
                            }
                            $file19 = "assets/conac/" . $dir . "/" . $current_dir . "/04_19/file.pdf";
                            if (curlExist("../../" . $file19)) {
                                $td19 = $file19;
                            } else {
                                $td19 = false;
                            }
                            $file20 = "assets/conac/" . $dir . "/" . $current_dir . "/01_18/file.pdf";
                            if (curlExist("../../" . $file20)) {
                                $td20 = $file20;
                            } else {
                                $td20 = false;
                            }
                            $file21 = "assets/conac/" . $dir . "/" . $current_dir . "/02_18/file.pdf";
                            if (curlExist("../../" . $file21)) {
                                $td21 = $file21;
                            } else {
                                $td21 = false;
                            }
                            $file22 = "assets/conac/" . $dir . "/" . $current_dir . "/03_18/file.pdf";
                            if (curlExist("../../" . $file22)) {
                                $td22 = $file22;
                            } else {
                                $td22 = false;
                            }
                            $file23 = "assets/conac/" . $dir . "/" . $current_dir . "/04_18/file.pdf";
                            if (curlExist("../../" . $file23)) {
                                $td23 = $file23;
                            } else {
                                $td23 = false;
                            }
                            $resultados[] = array("success"=> true, "tdd"=> str_replace("_", " ", $current_dir), "td0"=> $td0, "td1"=> $td1, "td2"=> $td2, "td3"=> $td3, "td4"=> $td4, "td5"=> $td5, "td6"=> $td6, "td7"=> $td7, "td8"=> $td8, "td9"=> $td9, "td10"=> $td10, "td11"=> $td11, "td12"=> $td12, "td13"=> $td13, "td14"=> $td14, "td15"=> $td15, "td16"=> $td16, "td17"=> $td17, "td18"=> $td18, "td19"=> $td19,"td20"=> $td20,"td21"=> $td21,"td22"=> $td22,"td23"=> $td23);
						}
					}
				}
			}
		}
	}
  print json_encode($resultados);
?>
