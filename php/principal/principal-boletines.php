<?php
	header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');

	//NOMBRE DE ARCHIVO
	$fileList = glob('../../assets/news/*_boletin_*.json');
	//ORDENAMOS EL ARREGLO DE ARCHIVOS POR FECHA
	natsort($fileList);
	$fileList = array_reverse($fileList, false);
	//RECORREMOS LOS ARCHIVOS n VECES
    $n = 0;
	foreach($fileList as $filename) {
	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {
            $n ++;
            if ($n <= 5) {
                $filename = file_get_contents($filename);
                $json = json_decode($filename, true);
                foreach ($json as $content) {
                    $date = $content['date'];
                    $dia = substr($date, 0,2);
                    $mes = substr($date, 2,2);
    
                    if ($mes == "1") { $mes == "ene";} elseif ($mes == "2") { $mes == "feb";} elseif ($mes == "3") { $mes == "mar";} elseif ($mes == "4") { $mes == "abr";} elseif ($mes == "5") { $mes == "may";} elseif ($mes == "6") { $mes == "jun";} elseif ($mes == "7") { $mes == "jul";} elseif ($mes == "8") { $mes == "ago";} elseif ($mes == "9") { $mes == "sep";} elseif ($mes == "10") { $mes == "oct";} elseif ($mes == "11") { $mes == "nov";} elseif ($mes == "12") { $mes == "dic";}
    
                    $ano = substr($date, 4,4);
                    $fecha = $dia."-".$mes."-".$ano;
                    $title = $content['title'];
                    $foto = $content['foto1'];
                    $folio = $content['folio'];
                    $random = $content['random'];
                    $file = $folio."_boletin_".$random."_".$date.".json";
                    $resultados[] = array("success"=> true, "fecha"=> $fecha, "titulo"=> stripslashes($title), "foto"=> $foto, "folio"=> $folio, "random"=> $random, "file"=> $file);
                }
            }
		} else {
            $resultados[] = array("success"=> false, "message"=> "Error de archivo " . error_get_last() );
		}
	}
    print json_encode($resultados);
?>