<?php
	header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
    include_once("../../functions/functions.php");

	$url = 'http://headless-wordpress-1/wp-json/wp/v2/posts/?categories=7';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: PHP'));
    if($response = curl_exec($ch)) {
        curl_close($ch);
        $data = json_decode($response);
        foreach($data as $item) {
            $id = $item->id;
            $title = $item->title->rendered;
            // DATE TO LOCAL TIME
            $fecha = date("d-m-Y", strtotime($item->date));
            $note = $item->content->rendered;
            $foto = imageEmbedded($item->id);
            $resultados[] = array("success"=> true, "fecha"=> $fecha, "titulo"=> $title, "nota"=> limit_text($note, 15), "foto"=> $foto, "id"=> $id);
        }
    }   else {
        $resultados[] = array("success"=> false, "message"=> "Fetch error " . error_get_last() );
    }
    print json_encode($resultados);


    // Get image from imageEmbedded()
    function imageEmbedded($id) {
        $media = "/img/logo-wide.png";
        $url = 'http://headless-wordpress-1/wp-json/wp/v2/posts/' . $id . '?_embed';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: PHP'));
        if($response = curl_exec($ch)) {
            curl_close($ch);
            $data = json_decode($response);
            if (isset($data->_embedded->{'wp:featuredmedia'}[0]->source_url)) {
                $media = $data->_embedded->{'wp:featuredmedia'}[0]->source_url;
            }
            return $media;
            
        }
        return false;
    }

?>