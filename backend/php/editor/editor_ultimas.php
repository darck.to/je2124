<?php
	header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
	
	//NOMBRE DE ARCHIVO
	$fileList = glob('../../../assets/news/*boletin*.json');

	//ORDENAMOS EL ARREGLO DE ARCHIVOS POR FOLIO
	natsort($fileList);
	$fileList = array_reverse($fileList, false);

	$resultados = array();
	//RECORREMOS LOS ARCHIVOS
	foreach($fileList as $filename){
	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {
			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);
			foreach ($json as $content) {
				
				$resultados[] = $content;
			}
		} else {
			$resultados[] = array("success"=> false, "message"=> "No se encontraron archivos " . error_get_last());
		}
	}
	print json_encode($resultados);
?>