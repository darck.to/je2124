<?php
	header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
	include('../../functions/abre_conexion.php');
	include('../../functions/functions.php');

	//INICIALIZAMOS RESULTADOS
	$resultados = array();

	if (empty($_POST['title'])) { $resultados[] = array("success"=> false, "message"=> "Falta Titulo"); }
	if (empty($_POST['note'])) { $resultados[] = array("success"=> false, "message"=> "Falta Cuerpo de la Nota"); }

	//LEEMOS EL ULTIMO FOLIO
	$folio = leeFolio();

	//RECIBIMOS LA INFORMACION
	$title = mysqli_real_escape_string($mysqli, $_POST['title']);
    $note = mysqli_real_escape_string($mysqli, $_POST['note']);
    date_default_timezone_set("America/Mexico_City");
	$fecha = Date('D-M-Y H:i');
	$date = Date('dmYHi');

	//ID ALEATORIO
	$random = generateRandomString();

	//GUARDAR IMAGENES EN CARPETA
    if (isset($_FILES['file1'])) {
	    $errors= array();
	    $file_tmp =$_FILES['file1']['tmp_name'];
	    $file_type=$_FILES['file1']['type'];
	    $file_ext='PNG';
	    //CAMBIAMOS EL NOMBRE DEL ARCHIVO A UNO MAS CORTO Y DE ACUERDO A LA NOMENCLATURA DEL IDATT
	    $file_name_1 = "img_1_".$random."_".$date.".".$file_ext;
	    $extensions= array("PNG");
	    if (in_array($file_ext,$extensions)=== false) {
	      	$errors[]="extension not allowed, please choose a JPEG or PNG file.";
	    }
	    if (empty($errors)==true) {
			//LE CAMBIAMOS EL TAMAÑO A LA IMAGEN ANTES DE GUARDARLA EN LA BDD
			$file_name_final_1 = "../../../assets/news//img/".$file_name_1;
			$source_img = $file_tmp;
			$destination_img = $file_name_final_1;
			//LLAMAMOS LA FUNCION PARA CAMBIARLE EL TAMAÑO
			$file_compressed = compress($source_img, $destination_img, 80);
			//LA GUARDAMOS EN EL DIRECTORIO CORRESPONDIENTE
			move_uploaded_file($file_compressed,".".$file_name_1);
			$resultados[] = array("success"=> true, "message"=> "Se Subio Imagen Numero 1");
	    } else {
			//print_r($errors);
			$resultados[] = array("success"=> false, "message"=> "No se puedo subir la imagen 1, consulta a soporte " . $errors);
	    }
	} else {
	  	//echo "Ocurrio un error, favor de consultar al administrador";
		$resultados[] = array("success"=> false, "message"=> "No se puedo subir la imagen 1, consulta a soporte " . $errors);
	  	$file_name_final_1 = "";
	}
	if (isset($_FILES['file2'])) {
	    $errors= array();
	    $file_tmp =$_FILES['file2']['tmp_name'];
	    $file_type=$_FILES['file2']['type'];
	    $file_ext='PNG';
	    //CAMBIAMOS EL NOMBRE DEL ARCHIVO A UNO MAS CORTO Y DE ACUERDO A LA NOMENCLATURA DEL IDATT
	    $file_name_2 = "img_2_".$random."_".$date.".".$file_ext;
	    $extensions= array("PNG");
	    if (in_array($file_ext,$extensions) === false) {
	      	$errors[]="extension not allowed, please choose a JPEG or PNG file.";
	    }
	    if (empty($errors)==true) {
			//LE CAMBIAMOS EL TAMAÑO A LA IMAGEN ANTES DE GUARDARLA EN LA BDD
			$file_name_final_2 = "../../../assets/news//img/".$file_name_2;
			$source_img = $file_tmp;
			$destination_img = $file_name_final_2;
			//LLAMAMOS LA FUNCION PARA CAMBIARLE EL TAMAÑO
			$file_compressed = compress($source_img, $destination_img, 80);
			//LA GUARDAMOS EN EL DIRECTORIO CORRESPONDIENTE
			move_uploaded_file($file_compressed,".".$file_name_2);
			//echo "Imagen Subida!";
			$resultados[] = array("success"=> true, "message"=> "Se Subio Imagen Numero 2");
	    } else {
			$resultados[] = array("success"=> false, "message"=> "No se puedo subir la imagen 2, consulta a soporte");
	      	//print_r($errors);
	    }
	} else {
	  	//echo "Ocurrio un error, favor de consultar al administrador";
		$resultados[] = array("success"=> false, "message"=> "No se puedo subir la imagen 2, consulta a soporte");
	  	$file_name_final_2 = "";
	}
	//GUARDAMOS EL ARCHIVO DE LA NOTA
    $cabecera[] = array('folio'=> $folio,'fc'=> $fecha, 'random'=> $random, 'date'=> $date, 'title'=> $title, 'note'=> $note, 'foto1'=> $file_name_1, 'foto2'=> $file_name_2, 'portada'=> 0);
	//NOMBRE DE ARCHIVO
	$fileName = '../../../assets/news/'.$folio.'_boletin_'.$random.'_'.$date.'.json';
	//INCREMENTAMOS EL FOLIO
	masFolio();
	if (file_exists($fileName)) {
		$resultados[] = array("success"=> true, "message"=> "ocurrio un error al guardar la nota");
	} else {
		$fileChat = fopen($fileName, 'w') or die ('No se guardo el archivo');
		fwrite($fileChat, json_encode($cabecera, JSON_PRETTY_PRINT));
		fclose($fileChat);
		$resultados[] = array("success"=> true, "message"=> "Nota Guardada");
		// Guardado en la base de datos
		$string = $folio . "_boletin_" . $random . "_" . $date;
		if ($sqlDire = $mysqli->query("INSERT INTO `notas_table`(`titulo`, `cuerpo`, `folio`) VALUES ('$title', '$note', '$string')")) {
			$resultados[] = array('success'=>true, "message"=> "Insertado folio: " . $string);    
		} else {
			$resultados[] = array("success"=> false, "error"=> mysqli_error($mysqli));
		}
	}
	include('../../functions/cierra_conexion.php');
	print json_encode($resultados);
?>