<?php
	include('../../functions/abre_conexion.php');	
	$_POST = json_decode(file_get_contents('php://input'), true);

	//RECIBIMOS LA INFORMACION
	$action = mysqli_real_escape_string($mysqli, $_POST['action']);
    $file = mysqli_real_escape_string($mysqli, $_POST['file']);

    //LEEMOS LA ACCIÓN
    if ($action == 2) {

    	$jsonString = file_get_contents('../../../assets/news/'.$file);
		$data = json_decode($jsonString, true);

		$resultados = array();

		$resultados[] = $file;
		$resultados[] = $data[0]['date'];
		$resultados[] = $data[0]['title'];
		$resultados[] = $data[0]['note'];
		$resultados[] = $data[0]['foto1'];
		$resultados[] = $data[0]['foto2'];

		print json_encode($resultados, JSON_PRETTY_PRINT);

	//BORRAR BOLETÍN PERMANENTEMENTE
    }

   include('../../functions/cierra_conexion.php');

?>