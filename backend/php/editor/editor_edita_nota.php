<?php
	header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
	include('../../functions/abre_conexion.php');
	include('../../functions/functions.php');

	//INICIALIZAMOS RESULTADOS
	$resultados = array();

	//RECIBIMOS LA INFORMACION
	$action = mysqli_real_escape_string($mysqli, $_POST['action']);
    $file = mysqli_real_escape_string($mysqli, $_POST['file']);

    //LEEMOS LA ACCIÓN
    //SI ES 1 LA MARCAMOS COMO FAVORITA PARA PORTADA (SOLO PUEDE HABER 3)
    if ($action == 1) {
    	$jsonString = file_get_contents('../../../assets/news/'.$file);
		$data = json_decode($jsonString, true);
		$viejo = $data[0]['portada'];
		//LEÉMOS EL VALOR Y LO CAMBIAMOS EN CONTRA
		if ($data[0]['portada'] == 1) {
			$portada = 0;
		} elseif ($data[0]['portada'] == 0) {
			$portada = 1;
		}
		$data[0]['portada'] = $portada;
		//LO VOLVEMOS A GUARDAR
		$newJsonString = json_encode($data, JSON_PRETTY_PRINT);
		if (file_put_contents('../../../assets/news/'.$file, $newJsonString)) {
			$resultados[] = array("success"=> true, "message"=> "Se destaco la nota");
		} else {
			$resultados[] = array("success"=> false, "message"=> "No se pudo destacar la nota");
		}
	//EDITAR NOTA
	} elseif ($action == 2) {
		//RECIBIMOS LA INFORMACION
		$file = mysqli_real_escape_string($mysqli, $_POST['file']);
		$title = mysqli_real_escape_string($mysqli, $_POST['title']);
		$note = mysqli_real_escape_string($mysqli, $_POST['note']);
		$date = Date('dmYHi');
		//INICIALIZAMOS INDICADORES EN FALSE
		$foto1 = false;
		$foto2 = false;
		//GUARDAR IMAGENES EN CARPETA
		if (isset($_FILES['file1'])) {
			$errors= array();
			$file_tmp =$_FILES['file1']['tmp_name'];
			$file_type=$_FILES['file1']['type'];
			$file_ext='PNG';
			//CAMBIAMOS EL NOMBRE DEL ARCHIVO A UNO MAS CORTO Y DE ACUERDO A LA NOMENCLATURA DEL IDATT
			$file_name_1 = "img_1_".$random."_".$date.".".$file_ext;
			$extensions= array("PNG");
			if (in_array($file_ext,$extensions)=== false) {
				$errors[]="extension not allowed, please choose a JPEG or PNG file.";
			}
			if (empty($errors)==true) {
				//LE CAMBIAMOS EL TAMAÑO A LA IMAGEN ANTES DE GUARDARLA EN LA BDD
				$file_name_final_1 = "../../../assets/news//img/".$file_name_1;
				$source_img = $file_tmp;
				$destination_img = $file_name_final_1;
				//LLAMAMOS LA FUNCION PARA CAMBIARLE EL TAMAÑO
				$file_compressed = compress($source_img, $destination_img, 80);
				//LA GUARDAMOS EN EL DIRECTORIO CORRESPONDIENTE
				move_uploaded_file($file_compressed,".".$file_name_1);
				$resultados[] = array("success"=> true, "message"=> "Se Subio Imagen Numero 1");
				$foto1 = true;
			} else {
				$resultados[] = array("success"=> false, "message"=> "No se puedo subir la imagen 1, consulta a soporte " . $errors);
			}
		} else {
			$resultados[] = array("success"=> true, "message"=> "No se edito la imagen 1");
		}
		if (isset($_FILES['file2'])) {
			$errors= array();
			$file_tmp =$_FILES['file2']['tmp_name'];
			$file_type=$_FILES['file2']['type'];
			$file_ext='PNG';
			//CAMBIAMOS EL NOMBRE DEL ARCHIVO A UNO MAS CORTO Y DE ACUERDO A LA NOMENCLATURA DEL IDATT
			$file_name_2 = "img_2_".$random."_".$date.".".$file_ext;
			$extensions= array("PNG");
			if (in_array($file_ext,$extensions) === false) {
				$errors[]="extension not allowed, please choose a JPEG or PNG file.";
			}
			if (empty($errors)==true) {
				//LE CAMBIAMOS EL TAMAÑO A LA IMAGEN ANTES DE GUARDARLA EN LA BDD
				$file_name_final_2 = "../../../assets/news//img/".$file_name_2;
				$source_img = $file_tmp;
				$destination_img = $file_name_final_2;
				//LLAMAMOS LA FUNCION PARA CAMBIARLE EL TAMAÑO
				$file_compressed = compress($source_img, $destination_img, 80);
				//LA GUARDAMOS EN EL DIRECTORIO CORRESPONDIENTE
				move_uploaded_file($file_compressed,".".$file_name_2);
				//echo "Imagen Subida!";
				$resultados[] = array("success"=> true, "message"=> "Se Subio Imagen Numero 2");
				$foto2 = true;
			} else {
				$resultados[] = array("success"=> false, "message"=> "No se puedo subir la imagen 2, consulta a soporte " . $errors);
			}
		} else {
			$resultados[] = array("success"=> true, "message"=> "No se edito la imagen 2");
		}
		//GUARDAMOS EL ARCHIVO DE LA NOTA
		
		$fileName = '../../../assets/news/' . $file;
		//MODIFICAMOS
		$jsonString = file_get_contents('../../../assets/news/'.$file);
		$data = json_decode($jsonString, true);
		//LEÉMOS EL VALOR Y LO CAMBIAMOS EN CONTRA
		$data[0]['title'] = $title;
		$data[0]['note'] = $note;
		$data[0]['title'] = $title;
		//SI SE EDITA IMAGENES, SE GUARDAN IMAGENES
		if ($foto1 == true) {
			$data[0]['foto1'] = $$file_name_1;
		}
		if ($foto2 == true) {
			$data[0]['foto2'] = $$file_name_2;
		}
		//LO VOLVEMOS A GUARDAR
		$newJsonString = json_encode($data, JSON_PRETTY_PRINT);
		if (file_put_contents('../../../assets/news/'.$file, $newJsonString)) {
			$resultados[] = array("success"=> true, "message"=> "Se edito la nota");
		} else {
			$resultados[] = array("success"=> false, "message"=> "No se pudo destacar la nota");
		}
	//ELIMINAR BOLETIN
    } elseif ($action == 3) {
    	if (unlink('../../../assets/news/'.$file)) {
			$resultados[] = array("success"=> true, "message"=> "Se eliminó la nota");
		} else {
			$resultados[] = array("success"=> false, "message"=> "No se pudo eliminar la nota");
		}
    }

    include('../../functions/cierra_conexion.php');
	print json_encode($resultados);
?>