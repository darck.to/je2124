$(function() {
    var editor = new MediumEditor('.editable')

	//CARGA LAS NOTAS EN EL LATERAL
	cargaUltimas()
	
	$('.guardaNota').on( 'click' , function() {
		var title = $('#title').val();
		var note = $('.editable').html();
		var $foto1 = $("#foto1");
		var $foto2 = $("#foto2")
		if (parseInt($foto1.get(0).files.length) > 2){
			toast('Se permite un máximo de 1 archivos por carga');
			return false
		}
		if (parseInt($foto2.get(0).files.length) > 2){
			toast('Se permite un máximo de 1 archivos por carga');
			return false
		}
		var formData = new FormData()
		formData.append("file1", document.getElementById('foto1').files[0]);
		formData.append("file2", document.getElementById('foto2').files[0]);
		formData.append("title", title);
		formData.append("note", note)
		$.ajax({
			type: 'post',
			url: 'php/editor/editor_nueva_nota.php',
			data: formData,
			processData: false,
    		contentType: false,
			async: true,
			dataType: 'json',
			crossDomain: true,
			context: document.body,
			cache: false,
			success: function(data) {
				$.each(data, function (name, value) {
					if (value.success) {
						$('#title').val('');
						$('.editable').html('');
						$('#foto1').val('');
						$('#foto2').val('');
						$('.file-name').html('');
						toast(value.message);
						cargaUltimas()
					} else {
						toast(value.message)
					}
				})
			},
			error: function(xhr, tst, err) {
				console.log(err)
			}
		})
	})
	
	//CARGA ULTIMAS NOTAS
	function cargaUltimas(e) {
		$.ajax({
			cache: 'false',
			url: "php/editor/editor_ultimas.php",
			async: true,
			dataType: 'json',
			crossDomain: true,
			context: document.body,
			cache: false,
			success: function(data) {
				var box = '<h6 class="white-text left">&Uacute;ltimas</h6>', content = '<h6 class="white-text left">Destacados</h6>';
				$.each(data, function (name, value) {
					//REVISA SI FOTOS TRUE
					var pic1 = '../assets/news/img/' + value.foto1;
					var pic2 = '../assets/news/img/' +value.foto2
					if (value.foto1 == "") pic1 = ""
					if (value.foto2 == "") pic2 = ""
					//ARCHIVO
					var folio = value.folio + '_boletin_' + value.random + '_' + value.date + '.json';
					//SI ES DESTACADA SE DESTACA
					if (value.portada == 1) {
						content += '<div class="box m-1 p-2 has-background-danger-light">';
							content += '<div class="media-content">';
								content += '<div class="content">';
									content +=  '<span class="is-size-7">' + value.title.slice(0,50) + '...</span>';
								content += '</div>';
							content += '</div>';
							if (value.portada == 1) {
								content += '<span class="px-2 edita-boletin handed" act=1 val="' + folio + '"><i class="fas fa-star iconDestacado"></i></span>'
							}
							content += '<span class="px-2 edita-boletin handed" val="' + folio + '" act=2><i class="far fa-edit"></i></span>';
						content += '</div>'
					}
					box += '<li>';
						box += '<div class="box m-1 p-2">';
							box += '<div class="media">';
								box += '<div class="media-left">';
									box += '<img class="is-rounded" src="' + pic1 + '" width="50">';
									box += '<img class="is-rounded" src="' + pic2 + '" width="50">';
								box += '</div>';
							box += '</div>';
							box += '<div class="media-content">';
								box += '<div class="content">';
									box +=  '<span>' + value.title + '</span>';
								box += '</div>';
							box += '</div>';
							if (value.portada == 1) {
								box += '<span class="px-2 edita-boletin handed" act=1 val="' + folio + '"><i class="fas fa-star iconDestacado"></i></span>'
							} else {
								box += '<span class="px-2 edita-boletin handed" act=1 val="' + folio + '"><i class="far fa-star"></i></span>'
							}
							box += '<span class="px-2 edita-boletin handed" val="' + folio + '" act=2><i class="far fa-edit"></i></span>';
							box += '<span class="px-2 edita-boletin handed" act=3 val="' + folio + '"><i class="fas fa-trash"></i></span>';
						box += '</div>';
					box += '</li>'
				})
				$('#notasLista').html(box);
				$('#notasLista').prepend('<ul>' + content + '</ul>');
				//PAJINATOR
				$('#notasLista').pagify(5, 'li')
				//IMG TO ZOOM
				$('.img-to-zoom').hover(function() {
					$(this).addClass('hover-transition')
				}, function() {
					$(this).removeClass('hover-transition')
				});
				//DESTACAR EL BOLETIN
				$('.edita-boletin').on( 'click' , function() {
					var action = $(this).attr('act');
					var file = $(this).attr('val');
					if (action == 1) {
						//MARCAR COMO FAVORITO PARA PORTADA
						$.ajax({
							method: 'post',
							url: 'php/editor/editor_edita_nota.php',
							data: { 
								action: action,
								file: file
							},
							async: true,
							dataType: 'json',
							crossDomain: true,
							context: document.body,
							cache: false,
							success: function(data) {
								$.each(data, function(name,value) {
									if (value.success) {
										toast(value.message);
										cargaUltimas()
									} else {
										toast(valie.message)
									}
								})
							},
							error: function(xhr, tst, err) {
							console.log(err);
							}
						});
					}
					//EDITAR BOLETIN
					if (action == 2) {
						$.ajax({
							method: 'post',
							url: 'php/editor/editor_edita_boletin.php',
							data: JSON.stringify({ 
								action: action,
								file: file
							}),
							contentType: "application/json",
							context: document.body,
							success: function(data) {
								data = JSON.parse(data);
								$('#file').val(data[0]);
								$('#title').val(data[2]);
								$('#title').focus();
								$('.editable').html(data[3]);
								$('.img1').attr('src','../assets/news/img/' + data[4]);
								$('.img1').css('display','block');
								$('.img2').attr('src','../assets/news/img/' + data[5]);
								$('.img2').css('display','block');
								$('#foto1').prop('disabled',true);
								$('#foto2').prop('disabled',true);
								$('.btn').addClass('grey');
								$('.delImg1').css('display','block').attr('val',data[4]);
								$('.delImg2').css('display','block').attr('val',data[5]);
								//AGREGA EL BOTON DE EDICION Y BORRA EL DE GUARDADO NUEVO
								$('.guardaNota').css('display','none');
								$('.editaNota').css('display','block')
								//EDITOR DE NOTAS AJAX
								$('.editaNota').on('click', function (e) {
									var file = $('#file').val();
									var title = $('#title').val();
									var note = $('.editable').html();
									var $foto1 = $("#foto1");
									var $foto2 = $("#foto2")
									if (parseInt($foto1.get(0).files.length) > 2){
										toast('Se permite un máximo de 1 archivos por carga');
										return false
									}
									if (parseInt($foto2.get(0).files.length) > 2){
										toast('Se permite un máximo de 1 archivos por carga');
										return false
									}
									var formData = new FormData()
									formData.append("file1", document.getElementById('foto1').files[0]);
									formData.append("file2", document.getElementById('foto2').files[0]);
									formData.append("action", 2);
									formData.append("file", file);
									formData.append("title", title);
									formData.append("note", note)
									$.ajax({
										type: 'post',
										url: 'php/editor/editor_edita_nota.php',
										data: formData,
										processData: false,
										contentType: false,
										async: true,
										dataType: 'json',
										crossDomain: true,
										context: document.body,
										cache: false,
										success: function(data) {
											$.each(data, function (name, value) {
												if (value.success) {
													$('#title').val('');
													$('.editable').html('');
													$('#foto1').val('');
													$('#foto2').val('');
													$('.file-name').html('');
													toast(value.message);
													cargaUltimas()
												} else {
													toast(value.message)
												}
											})
										},
										error: function(xhr, tst, err) {
											console.log(err)
										}
									})
								})
							},
							error: function(xhr, tst, err) {
								console.log(err)
							}
						});
					}
					//ELIMINAR BOLETÍN
					if (action == 3) {
						var c = confirm("¿Deseas eliminar este bolet\xEDn? \nEsta acci\xF3n es irreversible");
						if (c == true) {
							$.ajax({
								method: 'post',
								url: 'php/editor/editor_edita_nota.php',
								data: { 
									action: action,
									file: file
								},
								async: true,
								dataType: 'json',
								crossDomain: true,
								context: document.body,
								cache: false,
								success: function(data) {
									$.each(data, function(name,value) {
										if (value.success) {
											toast(value.message);
											cargaUltimas()
										} else {
											toast(valie.message)
										}
									})
								},
								error: function(xhr, tst, err) {
								console.log(err);
								}
							});
						} else {
							return false;
						}
					}
				});
			},
			error: function(xhr, tst, err) {
			  console.log(err)
			}
		})
	}
	
	$('.clearBoletin').on( 'click' , function() {
		$('#file').val('');
		$('#date').val('');
		$('#title').val('');
		$('.editable').html('');
		$('#foto1').val('');
		$('#foto2').val('');
		$('.file-name').html('');
		$('.img1').attr('src','');
		$('.img2').attr('src','');
		$('#foto1').prop('disabled',false);
		$('#foto2').prop('disabled',false);
		$('.del-img').css('display','none').attr('val','');
		$('.img1').css('display','none');
		$('.img2').css('display','none');
		$('.guardaNota').css('display','block');
		$('.editaNota').css('display','none')
	})
	
	$('.delImg1').on( 'click' , function() {
		var c = confirm("¿Deseas eliminar esta imagen? \nEsta acci\xF3n es irreversible")
		if (c == true) {
			$('.img1').attr('src','');
			$('.img1').css('display','none');
			$('.delImg1').css('display','none').attr('val','');
			$('#foto1').prop('disabled',false)
		} else {
			return false
		}
	})
	
	$('.delImg2').on( 'click' , function() {
		var c = confirm("¿Deseas eliminar esta imagen? \nEsta acci\xF3n es irreversible")
		if (c == true) {
			$('.img2').attr('src','');
			$('.img2').css('display','none');
			$('.delImg2').css('display','none').attr('val','');
			$('#foto2').prop('disabled',false)
		} else {
			return false
		}
	})

	const fileInput1 = document.querySelector('.file-uploader_1 input[type=file]')
	fileInput1.onchange = () => {
		if (fileInput1.files.length > 0) {
			const fileName = document.querySelector('.file-uploader_1 .file-name');
			fileName.textContent = fileInput1.files[0].name
		}
	}

	const fileInput2 = document.querySelector('.file-uploader_2 input[type=file]')
	fileInput2.onchange = () => {
		if (fileInput2.files.length > 0) {
			const fileName = document.querySelector('.file-uploader_2 .file-name');
			fileName.textContent = fileInput2.files[0].name
		}
	}
})
  