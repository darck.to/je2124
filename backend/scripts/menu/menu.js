$(function() {
  //HAY LOGIN?
  var xer_log = localStorage.getItem("xer_log");
  //COMPROBAMOS SI EXISTE LOGIN LOCAL
  switch(xer_log) {
    case "0":
      //NO LOGIN
      $('.user-name').html('Usuarios');
      $('.user-name').removeAttr('href');
      carga_menu_options(0);
      break;
    case null:
      //VARIABLE NO ESTABLECIDA
      $('.user-name').html('Usuarios');
      $('.user-name').removeAttr('href');
      carga_menu_options(0);
      break;
    case "1":
      //LOGIN LOCAL
      carga_menu_options(1);
      //CARGA EL CARRITO EN LAS OPCIONES DEL MENU
      break;
  }

  $(".navbar-burger").click(function() {
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");
  })


  //FUNCTION CARGA OPCIONES DE MENU DE USUARIO
  function carga_menu_options(e) {
    var xer_key = localStorage.getItem("xer_key");
    var xer_user = localStorage.getItem("xer_user");
    var opc = e;
    $.ajax({
      type: 'POST',
      url: 'php/menu/menu-carga-options.php',
      data: {
        auth : xer_key,
        user : xer_user,
        opc : opc
      },
      async: true,
      dataType: 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        var profile = '', buttons = '';
        $.each(data, function (name, value) {
          if (value.success == true) {
            if (value.level == 0) {
              profile += '<a class="navbar-item profile-nav ' + value.class + '" href="' + value.ref + '">' + value.txt + '</a>';
            }
            if (value.level == 9) {
              profile += '<hr class="' + value.class + '">';
            }
            if (value.level == 1) {
              buttons += '<a class="button profile-nav ' + value.class + '" href="' + value.ref + '">' + value.txt + '</a>';
            }
            if (value.level == 8) {
              buttons += '<a class="button profile-logout ' + value.class + '" href="' + value.ref + '">' + value.txt + '</a>';
            }
          } else {
            $.ajax({
              type: 'POST',
              url: 'php/init/init-start.php',
              data: {
                a: 0
              },
              cache: false,
              success: function(data) {
                localStorage.removeItem("xer_key");
                localStorage.removeItem("xer_user");
                localStorage.removeItem("xer_log");
                window.location.href = '.'
              },
              error: function(xhr, tst, err) {
                toast(err)
              }
            })
          }
        })
        $('#perfil-options').html(profile);
        $('#perfil-buttons').html(buttons);

        //NAVEGADOR DE PERFIL
        $('.profile-nav').on('click', function(event) {
          event.preventDefault();
          var dest = $(this).attr('href')
          $.ajax({
            type: 'POST',
            url: 'php/perfil/perfil-inicia.php',
            data: {
              auth : xer_key,
              user : xer_user,
              dest : dest
            },
            async: true,
            dataType: 'json',
            crossDomain: true,
            context: document.body,
            cache: false
          })
          .done(function(data) {
            var destino = '';
            var blank = 0;
            $.each(data, function (name, value) {
              if (value.success) {
                blank = value.blank;
                destino = value.destino
              }
            })
            if (blank == 0) {
              modal();
              $('.modal-content').load(destino)
            } else {
              window.location.href = destino
            }
          })
          .fail(function() {
            console.log("error");
          })
        })

        //SESSION CLOSE
        $('.profile-logout').on('click', function(event){
          event.preventDefault();
          //CIERRA SESION PHP
          $.ajax({
            type: 'POST',
            url: 'php/init/init-start.php',
            data: {
              a: 0
            },
            cache: false,
            success: function(data) {
              localStorage.removeItem("xer_key");
              localStorage.removeItem("xer_user");
              localStorage.removeItem("xer_log");
              window.location.href = '.'
            },
            error: function(xhr, tst, err) {
              toast(err)
            }
          })
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  }

})
