<?php
	//GENERADOR DE CADENAS ALEATORIAS
	function generateRandomString($length = 10) {
	    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	//CAMBIAMOS PESO DE LAS IMAGENES
	function compress($source, $destination, $quality) {

		$info = getimagesize($source);
		if ($info['mime'] == 'image/jpeg') 
		  $image = imagecreatefromjpeg($source);
		elseif ($info['mime'] == 'image/gif') 
		  $image = imagecreatefromgif($source);
		elseif ($info['mime'] == 'image/png') 
		  $image = imagecreatefrompng($source);
		imagejpeg($image, $destination, $quality);
		return $destination;
	}

	function leeFolio() {
		//NOMBRE DE ARCHIVO
		$filename = '../../../assets/news/folio.json';
		if (file_exists($filename)) {
			$filename = file_get_contents($filename);
			$data = json_decode($filename, true);
			$folio = $data[0]['folio'];
		}
		return $folio;
	}

	function masFolio() {
		$filename = file_get_contents('../../../assets/news/folio.json');
		$data = json_decode($filename, true);
		$viejo = $data[0]['folio'];
		$data[0]['folio'] = $viejo + 1;
		//LO VOLVEMOS A GUARDAR
		$newJsonString = json_encode($data);
		file_put_contents('../../../assets/news/folio.json', $newJsonString);
	}
	
?>
