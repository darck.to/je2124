$(function() {
  //CARGA EL MAIN LOGIN
  carga_main_login()

  function carga_main_login(e) {
    var xer_key = localStorage.getItem("xer_key");
    var xer_user = localStorage.getItem("xer_user");
    var xer_log = localStorage.getItem("xer_log");
    //COMPROBAMOS SI EXISTE LOGIN LOCAL
    if (xer_log == 0 || xer_log == null) {
      //LOCAL LOGIN NULL OR NEGATIVE
      $.ajax({
        type: 'POST',
        url: 'php/init/init-comprueba-auth.php',
        data: {
          auth : xer_key,
          user : xer_user
        },
        async: true,
        dataType : 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
        success: function(data) {
          var aprobacion
          $.each(data, function (name, value) {
            if (value.success == true) {
              localStorage.setItem("xer_log",1);
              //INICIALIZA EL MENU
              cargaMenu();
              toast(value.message)
            } else {
              //INICIALIZA EL MENU
              cargaMenu();
              localStorage.removeItem("xer_key");
              localStorage.removeItem("xer_user");
              localStorage.setItem("xer_log",0)
            }
          });
        },
        error: function(xhr, tst, err) {
          toast('El tiempo de espera fue superado, por favor intentalo en un momento mas')
        }
      })
    } else {
      //INICIALIZA EL MENU
      cargaMenu()
    }
  }
  function cargaMenu(e) {
    $('#menu').load('templates/menu/menu.html')
  }
});
