// Avoid `console` errors in browsers that lack a console.
(function() {
  var method;
  var noop = function () {};
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  }
}());

// Place any jQuery/helper plugins in here.
$(function() {
  //CARGA NAVIGATION
  window.navigation = function (x) {
    var nav = 'templates/' + x + '/' + x + '.html';
    $('.main-container').load(nav)
  }
  //CREA UN TOAST MODAL
  window.toast = function(e) {
    if (!$('#toast').length) {
      $('body').append('<div id="toast"></div>');
    }
    var toast = '<span class="has-background-info has-text-white is-size-7 border-r pa-qrt ma-half toast-tag">' + e + '</span>';
    $('#toast').append(toast);
    setTimeout(function(){
      if ($('.toast-tag').length > 0) {
        $('.toast-tag').fadeOut('fast', function() {
          $(this).remove()
        })
      }
    }, 4000)
  }
  //CREA UN MODAL PARA TODOS Y TODO
  window.modal = function(incoming) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    }
    //CREA UN MODAL NUEVO
    var content = incoming;
    var modal = '<div class="modal">';
    modal += '<div class="modal-background"></div>';
    modal += '<div class="modal-content">';
      modal += '<div class="box has-background-white">';
        modal += content;
      modal += '</div>';
    modal += '</div>';
    modal += '<div class="modal-absolute"></div>';
    modal += '<button class="modal-close is-large" aria-label="close"></button>';
    modal += '</div>';
    $('body').prepend(modal);
    $('.modal').css('z-index','9000');
    $('.modal').toggleClass("is-active");
    $('.modal-close').on('click', function(){
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    });
    $('.modal-background').on('click', function(){
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    })
  }
  //MODAL SOLO CLAUSURABLE CON BOTON
  window.modap = function(incoming) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    }
    //CREA UN MODAL NUEVO
    var content = incoming;
    var modal = '<div class="modal">';
    modal += '<div class="modal-background"></div>';
    modal += '<div class="modal-content">';
      modal += '<div class="box has-background-white">';
        modal += content;
      modal += '</div>';
    modal += '</div>';
    modal += '<div class="modal-absolute"></div>';
    modal += '<button class="modal-close is-large" aria-label="close"></button>';
    modal += '</div>';
    $('body').prepend(modal);
    $('.modal').css('z-index','9000');
    $('.modal').toggleClass("is-active");
    $('.modal-close').on('click', function(){
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    })
  }
  //CIERRAMODAL
  window.xmodal = function(e) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    }
  }
  //MONEY FORMAT
  window.money_format = function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  //LOADING STUFF
  window.startLoader = function(x) {
    $('.loader').addClass('is-loading');
  }
  window.stopLoader = function(x) {
    $('.loader').removeClass('is-loading');
  }

  //CONSULTA CARRITO
  window.actualCarrito = function(e) {
    var form_data = new FormData();
    var form_method = 'post';
    var form_url = 'php/carrito/carrito-carga-actual.php';
    var tie_key = localStorage.getItem("tie_key");
    var tie_user = localStorage.getItem("tie_user");
    form_data.append('auth',tie_key);
    form_data.append('user', tie_user)
    return $.ajax({
        url: form_url,
        method: form_method,
        data: form_data,
        contentType: false,
        processData: false,
        async: true,
        dataType: 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
    })
  }
  //AGREGA UN CARRITO
  window.agregaCarrito = function(t,p,c) {
    var tienda = t;
    var producto = p;
    var cantidad = c;
    var form_data = new FormData();
    var form_method = 'post';
    var form_url = 'php/carrito/carrito-agrega-producto.php';
    var tie_key = localStorage.getItem("tie_key");
    var tie_user = localStorage.getItem("tie_user");
    form_data.append('auth',tie_key);
    form_data.append('user', tie_user);
    form_data.append('tienda', tienda);
    form_data.append('producto', producto);
    form_data.append('cantidad', cantidad);
    return $.ajax({
        url: form_url,
        method: form_method,
        data: form_data,
        contentType: false,
        processData: false,
        async: true,
        dataType: 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
    })
  }
  //REVISA INVENTARIO DE CARRITO
  window.revisaSiMayorCarrito = function(c,t,i) {
    var carrito = c;
    var transaccion = t;
    var cantidad = i;
    var form_data = new FormData();
    var form_method = 'post';
    var form_url = 'php/carrito/carrito-revisasi-existencias.php';
    var tie_key = localStorage.getItem("tie_key");
    var tie_user = localStorage.getItem("tie_user");
    form_data.append('auth',tie_key);
    form_data.append('user', tie_user);
    form_data.append('carrito', carrito);
    form_data.append('transaccion', transaccion);
    form_data.append('cantidad', cantidad)
    return $.ajax({
        url: form_url,
        method: form_method,
        data: form_data,
        contentType: false,
        processData: false,
        async: true,
        dataType: 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
    })
  }
  window.revisaInventarioCarrito = function(c,t) {
    var carrito = c;
    var transaccion = t;
    var form_data = new FormData();
    var form_method = 'post';
    var form_url = 'php/carrito/carrito-revisa-existencias.php';
    var tie_key = localStorage.getItem("tie_key");
    var tie_user = localStorage.getItem("tie_user");
    form_data.append('auth',tie_key);
    form_data.append('user', tie_user);
    form_data.append('carrito', carrito);
    form_data.append('transaccion', transaccion)
    return $.ajax({
        url: form_url,
        method: form_method,
        data: form_data,
        contentType: false,
        processData: false,
        async: true,
        dataType: 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
    })
  }
})