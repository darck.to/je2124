$(function() {
	
	//cargaComunicados();
    cargaWP();

    function cargaComunicados(e) {
        var box = "";
        //CARGA INFORMACION DE BOLETINES
        $.ajax({
            url: 'php/comunicados/comunicados-lista.php',
            async: true,
            dataType : 'json',
            crossDomain: true,
            context: document.body,
            cache: false,
            success: function(data) {
                $.each(data, function (name, value) {
                    if (value.success) {
                        //CARGA LA PLANTILLA DE LAS FICHAS
                        box +='<div class="column is-3">';
                            box +='<div class="card">';
                                box +='<div class="card-image">';
                                    box +='<figure class="image is-3by2">';
                                        box +='<a href="nota.php?nota=' + value.file + '"><img src="assets/news/img/' + value.foto + '" alt="' + value.titulo + '"></a>';
                                    box +='</figure>';
                                box +='</div>';
                                box +='<div class="card-content">';
                                    box +='<div class="media">';
                                        box +='<div class="media-content">';
                                            box +='<a href="nota.php?nota=' + value.file + '"><h1 class="is-size-7 has-text-grey-dark">' + value.titulo + '</h1></a>';
                                            box +='<p class="is-size-7 pt-2 has-text-grey-dark">' + value.nota + '</p>';
                                            box +='<a href="https://www.facebook.com/gobjerez" target="_blank">@gobjerez</a>';
                                        box +='</div>';
                                    box +='</div>';
                                    box +='<div class="content">';
                                        box +='<time class="is-size-7" datetime="2016-1-1">' + value.fecha + '</time>';
                                    box +='</div>';
                                box +='</div>';
                            box +='</div>';
                        box +='</div>'
                    } else {
                        console.log(value.message)
                    }
                });
                $('#comunicadosLista').html(box);
                //PAJINATOR
		  	    $('#comunicadosLista').pagify(12, '.column')
            },
            error: function(xhr, tst, err) {
            console.log(err)
            }
        })
    }

    function cargaWP(e) {
        var box = "";
        //CARGA INFORMACION DE BOLETINES
        $.ajax({
            url: 'php/comunicados/comunicados-wp.php',
            async: true,
            dataType : 'json',
            crossDomain: true,
            context: document.body,
            cache: false,
            success: function(data) {
                $.each(data, function (name, value) {
                    if (value.success) {
                        //CARGA LA PLANTILLA DE LAS FICHAS
                        box +='<div class="column is-3">';
                            box +='<div class="card">';
                                box +='<div class="card-image">';
                                    box +='<figure class="image is-3by2">';
                                        box +='<a href="wp.php?id=' + value.id + '"><img src="' + value.foto + '" alt="' + value.titulo + '"></a>';
                                    box +='</figure>';
                                box +='</div>';
                                box +='<div class="card-content">';
                                    box +='<div class="media">';
                                        box +='<div class="media-content">';
                                            box +='<a href="wp.php?id=' + value.id + '"><h1 class="is-size-7 has-text-grey-dark">' + value.titulo + '</h1></a>';
                                            box +='<p class="is-size-7 pt-2 has-text-grey-dark">' + value.nota + '</p>';
                                            box +='<a href="https://www.facebook.com/gobjerez" target="_blank">@gobjerez</a>';
                                        box +='</div>';
                                    box +='</div>';
                                    box +='<div class="content">';
                                        box +='<time class="is-size-7" datetime="2016-1-1">' + value.fecha + '</time>';
                                    box +='</div>';
                                box +='</div>';
                            box +='</div>';
                        box +='</div>'
                    } else {
                        console.log(value.message)
                    }
                });
                $('#comunicadosLista').append(box);
                //PAJINATOR
		  	    $('#comunicadosLista').pagify(12, '.column')
            },
            error: function(xhr, tst, err) {
            console.log(err)
            }
        })
    }
	
	
})