$(function() {

	cargaArchivo();

  function cargaArchivo(e) {
      var box = "";
      //CARGA INFORMACION DE BOLETINES
      $.ajax({
          url: 'php/archivo/archivo-carga.php',
          async: true,
          dataType : 'json',
          crossDomain: true,
          context: document.body,
          cache: false,
          success: function(data) {
              $.each(data, function (name, value) {
                  if (value.success) {
                      //CARGA LA PLANTILLA DE LAS FICHAS
                      box += '<tr>';
                          box += '<td>' + decodeURIComponent(escape(value.nombre)).toLowerCase().replace(/^(.)|\s+(.)/g, c => c.toUpperCase()); + '</td>';
                          box += '<td class="has-text-centered"><a href="' + value.url + '" target="_blank" class="has-color-main"><i class="far fa-file-pdf"></i></a></td>';
                      box += '</tr>'
                  } else {
                      console.log(value.message)
                  }
              });
              $('#archivoTable').html(box)
          },
          error: function(xhr, tst, err) {
              console.log(err)
          }
      })
    }

})
