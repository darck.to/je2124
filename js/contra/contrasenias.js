$(function() {

    $('#introduceClave').on('click', function(e) {
        e.preventDefault();
        var clave = $('#inputClave').val();
        cargaContrasenias(clave)
    });

    function cargaContrasenias(e) {
        var box = "";
        //CARGA INFORMACION DE BOLETINES
        $.ajax({
            url: 'php/contra/contra-carga.php',
            contentType: 'application/json',
            type: 'POST',
            dataType: 'html',
            data: JSON.stringify({
                clave : e
            }),
            success: function(data) {
                let resultado = JSON.parse(data);
                $.each(resultado, function (name, value) {
                    if (value.success) {
                        //CARGA LA PLANTILLA DE LAS FICHAS
                        box += '<b>' + value.clave + '</b>';
                    } else {
                        box += '<em>' + value.message + '</em>';
                        console.log(value.message)
                    }
                });
                $('#principalContrasenias').html(box)
            },
            error: function(xhr, tst, err) {
                console.log(err)
            }
        })
    }

})
