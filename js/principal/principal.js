$(function() {
    //CARGA BANNER PRINCIPAL
    cargaTiles();
    //CARGA TIRA DE NOTAS
    cargaBoletines();
    // CARGA AVISOS DE PRIVACIDAD
    cargaAvisos();

    function cargaTiles(e) {
        var box = "";
        //CARGA INFORMACION DE BOLETINES
        $.ajax({
            url: 'php/principal/principal-tiles.php',
            async: true,
            dataType : 'json',
            crossDomain: true,
            context: document.body,
            cache: false,
            success: function(data) {
                if (data[0].success) {
                    //CARGA LA PLANTILLA DE LAS FICHAS
                    box += '<div class="tile is-parent is-vertical">';
                        box += '<article class="tile is-child notification has-background-white">';
                            box += '<a href="nota.php?nota=' + data[0].file + '"><p class="title is-size-4">' + data[0].titulo + '</p></a>';
                            box += '<a href="nota.php?nota=' + data[0].file + '"><p class="subtitle">' + data[0].nota + '</p></a>';
                            box += '<figure class="image is-4by3">';
                                box += '<a href="nota.php?nota=' + data[0].file + '"><img class="pt-1" src="assets/news/img/' + data[0].foto + '"></a>';
                            box += '</figure>';
                        box += '</article>';
                        box += '<article class="tile is-child notification has-background-white">';
                            box += '<a href="nota.php?nota=' + data[1].file + '" class="mt-4"><p class="title is-size-4 pt-1">' + data[1].titulo + '</p></a>';
                            box += '<a href="nota.php?nota=' + data[1].file + '" class="mt-4"><p class="subtitle">' + data[1].nota + '</p></a>';
                            box += '<p class="is-size-7"><a href="https://www.facebook.com/gobjerez" target="_blank">@gobjerez</a></p>';
                            box += '<p class="is-size-7"><a>#TransformandoJerez</a></p>';
                        box += '</article>';
                    box += '</div>';
                    box += '<div class="tile is-parent">';
                        box += '<article class="tile is-child notification has-background-white has-height-full-cont">';
                            box += '<figure class="image is-4by3">';
                                box += '<a href="nota.php?nota=' + data[2].file + '"><img src="assets/news/img/' + data[2].foto + '"></a>';
                            box += '</figure>';
                            box += '<a href="nota.php?nota=' + data[2].file + '"><p class="title is-size-4 pt-1">' + data[2].titulo + '</p></a>';
                            box += '<a href="nota.php?nota=' + data[2].file + '"><p class="subtitle">' + data[2].nota + '</p></a>';
                            box += '<div class="columns mt-0">';
                                box += '<div class="column is-offset-8 is-4"><img class="image" src="img/logo-header.png" height="120"></div>';
                            box += '</div>';
                        box += '</article></a>';
                    box += '</div>'
                } else {
                    console.log(value.message)
                }
                $('#bannerPrincipal').html(box)
            },
            error: function(xhr, tst, err) {
            console.log(err)
            }
        });
    }

    function cargaBoletines(e) {
        var box = "";
        //CARGA INFORMACION DE BOLETINES
        $.ajax({
            url: 'php/principal/principal-boletines.php',
            async: true,
            dataType : 'json',
            crossDomain: true,
            context: document.body,
            cache: false,
            success: function(data) {
                $.each(data, function (name, value) {
                    if (value.success) {
                        //CARGA LA PLANTILLA DE LAS FICHAS
                        box += '<a href="nota.php?nota=' + value.file + '"><div class="column">';
                            box +='<div class="card">';
                                box +='<div class="card-image">';
                                    box +='<figure class="image is-3by2">';
                                        box +='<img src="assets/news/img/' + value.foto + '" alt="' + value.titulo + '">';
                                    box +='</figure>';
                                box +='</div>';
                                box +='<div class="card-content">';
                                    box +='<div class="media">';
                                        box +='<div class="media-content">';
                                            box +='<h1 class="is-size-6 has-text-grey-dark">' + value.titulo + '</h1>';
                                            box +='<p class="is-size-7"><a href="https://www.facebook.com/gobjerez" target="_blank">@gobjerez</a></p>';
                                            box +='<p class="is-size-7"><a>#TransformandoJerez</a></p>';
                                        box +='</div>';
                                    box +='</div>';
                                    box +='<div class="content">';
                                        box +='<time class="is-size-7" datetime="2016-1-1 has-text-grey-dark">' + value.fecha + '</time>';
                                    box +='</div>';
                                box +='</div>';
                            box +='</div>';
                        box +='</div></a>'
                    } else {
                        console.log(value.message)
                    }
                });
                $('#boletinesPrincipal').html(box)
            },
            error: function(xhr, tst, err) {
            console.log(err)
            }
        });
    }

    // Carga regidores
    $('#regidoresSection').load('./templates/ayuntamiento/ayuntamiento.html #regidores');

    // Carga Avisos de Privacidad
    function cargaAvisos(e) {
        // Carga los avisos de 
        var box = "";
        //CARGA INFORMACION DE BOLETINES
        $.ajax({
            url: 'php/principal/principal-avisos.php',
            async: true,
            dataType : 'json',
            crossDomain: true,
            context: document.body,
            cache: false,
            success: function(data) {
                $.each(data, function (name, value) {
                    if (value.success) {
                        box += '<li class="has-text-light"><a href="' + value.url + '" target="_blank"><i class="fas fa-file-pdf pr-2 is-size-4"></i>' + value.nombre + '</a></li>';
                    } else {
                        console.log(value.message)
                    }
                });
                $('#avisosDePrivacidad').html(box);
            },
            error: function(xhr, tst, err) {
                console.log(err)
            }
        });
    }
});
  