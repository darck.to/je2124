$(document).ready(function () {
    $('form').submit(function(e) {
        e.preventDefault();
    })
    // Leemos el input llamado buscador
    $('#buscadorPrincipal').on('keyup', function (e) {
        // Limpiamos el resultado
        $('#resultadoCaja').html('');
        if (!$(this).val()) {
            $('#resultadoCaja').addClass('is-hidden');
            return false
        }
        // Guardamos el valor del input #buscadorPrincipal
        let valor = $(this).val().toLowerCase();
        // Buscamos el valor en el servidor de noticias
        $.ajax({
            type: 'POST',
            url: 'php/buscador/buscador-input.php',
            data: {valor: valor},
            async: true,
            dataType : 'json',
            context: document.body,
            cache: false,
            success: function (data) {
                // Mostramos el resultado
                // Traducimos el objecto a html
                // Recorremos el array
                $.each(data, function (index, value) {
                    if (value.success) {
                        // Muestra el box
                        $('#resultadoCaja').removeClass('is-hidden');
                        // Mostramos el contenido del archivo
                        $('#resultadoCaja').append('<li><a href="nota.php?nota=' + value['folio'] + '.json" class="is-hoverable"><b>' + value['titulo'] + '<b/>&nbsp;<em class="has-text-weight-light">' + value['cuerpo'] + '</a></em>')
                    } else {
                        // Oculat el box
                        $('#resultadoCaja').addClass('is-hidden');
                    }
                });
            }
        })
    })


});