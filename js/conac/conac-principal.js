$(function() {
	cargaConac();
	// Carga CONAC Cuenta Publica 2021
	cargaConacCuentaP()
});

function cargaConac(e) {
	var box = "";
	//CARGA INFORMACION DE BOLETINES
	$.ajax({
		url: 'php/conac/conac-carga.php',
		async: true,
		dataType : 'json',
		crossDomain: true,
		context: document.body,
		cache: false,
		success: function(data) {
			$.each(data, function (name, value) {
				if (value.success) {
					//CARGA LA PLANTILLA DE LAS FICHAS
					box += '<tr>';
						box += '<td class="has-text-left is-size-7">' + value.tdd + '</td>';
						if (value.td0) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td0 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td1) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td1 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td2) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td2 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td3) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td3 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td4) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td4 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td5) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td5 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td6) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td6 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td7) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td7 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td8) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td8 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td9) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td9 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td10) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td10 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td11) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td11 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td12) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td12 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td13) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td13 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td14) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td14 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td15) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td15 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td16) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td16 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td17) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td17 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td18) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td18 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td19) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td19 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td20) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td20 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td21) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td21 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td22) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td22 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
						if (value.td3) { box += '<td class="has-text-centered"><a target="_blank" href="' + value.td23 + '"><i class="fas fa-file-pdf"></i></a></td>'; } else { box += '<td class="has-text-centered has-text-grey-lighter"><i class="far fa-file-pdf"></i></td>'; }
					box += '</tr>'
				} else {
					console.log(value.message)
				}
			});
			$('#conacTabe').html(box)
		},
		error: function(xhr, tst, err) {
			console.log(err)
		}
	})
}

function cargaConacCuentaP(e) {
	$.post('php/conac/conac-carga-rdp.php', function(data) {
		txt = "<ul>"
		$.each(data, function (name, value) {
			if (value.success) {
				txt += '<li class="pb-1"><a target="_blank" href="' + value.link + '"><i class="fas fa-file-pdf has-text-grey"></i>&nbsp;<span class="has-text-danger-dark">' + value.file + '</span></a></li>';
			}
		})
		txt += '</ul>';
  	$('#conacRdp').html(txt)
	})

	$.post('php/conac/conac-carga-rdp22.php', function(data) {
		txt = "<ul>"
		$.each(data, function (name, value) {
			if (value.success) {
				txt += '<li class="pb-1"><a target="_blank" href="' + value.link + '"><i class="fas fa-file-pdf has-text-grey"></i>&nbsp;<span class="has-text-danger-dark">' + value.file + '</span></a></li>';
			}
		})
		txt += '</ul>';
  	$('#conacRdp22').html(txt)
	})

	$.post('php/conac/conac-carga-rdp23.php', function(data) {
		txt = "<ul>"
		$.each(data, function (name, value) {
			if (value.success) {
				txt += '<li class="pb-1"><a target="_blank" href="' + value.link + '"><i class="fas fa-file-pdf has-text-grey"></i>&nbsp;<span class="has-text-danger-dark">' + value.file + '</span></a></li>';
			}
		})
		txt += '</ul>';
  	$('#conacRdp23').html(txt)
	})
}
