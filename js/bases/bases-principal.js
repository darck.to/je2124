$(function() {

    cargaBases();

    function cargaBases(e) {
        //CARGA INFORMACION DE BOLETINES
        $.ajax({
            url: 'php/bases/bases-carga.php',
            async: true,
            dataType : 'json',
            crossDomain: true,
            context: document.body,
            cache: false,
            success: function(data) {
                let  box = "";
                $.each(data, function (name, value) {
                    if (value.success) {
                        //CARGA LA PLANTILLA DE LAS FICHAS
                        box += '<tr>';
                            box += '<td>' + decodeURIComponent(escape(value.nombre)).toUpperCase().replace(/^(.)|\s+(.)/g, c => c.toUpperCase()); + '</td>';
                            box += '<td class="has-text-centered"><a href="' + value.url + '" target="_blank" class="has-color-main"><i class="far fa-file-pdf"></i></a></td>';
                        box += '</tr>'
                    }
                });
                $('#basesTable').html(box)
            },
            error: function(xhr, tst, err) {
                console.log(err)
            }
        })
        // FONDOS
        $.post( "php/bases/fondos-carga.php", function(data) {
            let box = ""
            $.each(data, function (name, value) {
                if (value.success) {
                    //CARGA LA PLANTILLA DE LAS FICHAS
                    box += '<tr>';
                        box += '<td>' + decodeURIComponent(escape(value.nombre)).toLowerCase().replace(/^(.)|\s+(.)/g, c => c.toUpperCase()); + '</td>';
                        box += '<td class="has-text-centered"><a href="' + value.url + '" target="_blank" class="has-color-main"><i class="far fa-file-pdf"></i></a></td>';
                    box += '</tr>'
                }
            })
            $('#fondosTable').html(box)
        }, 'json')
    }

})
