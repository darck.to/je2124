$(function() {
    //IMG TO ZOOM
    $('.img-to-zoom').hover(function() {
        $(this).addClass('hover-transition')
    }, function() {
        $(this).removeClass('hover-transition')
    });
    //MODAL ZOOM
    $('.img-modal').on('click', function (e) {
        e.preventDefault();
        modal('<img src="' + $(this).attr('src') + '"/>');
        $('.modal-content').css('width', '70%')
    })

})
