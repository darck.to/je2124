<?php    
    $nota = $_GET['nota'];
	//NOMBRE DE ARCHIVO
	$filename = 'assets/news/'.$nota;
   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
	if (file_exists($filename)) {
		$filename = file_get_contents($filename);
		$json = json_decode($filename, true);
		foreach ($json as $content) {
			$date = $content['date'];
			$dia = substr($date, 0,2);
			$mes = substr($date, 2,2);
			$ano = substr($date, 4,4);
			$fecha = $dia."-".$mes."-".$ano;
			$title = $content['title'];
			$note = $content['note'];
			$foto1 = $content['foto1'];
			$foto2 = $content['foto2'];
		}
	}
?>
<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <?php
    echo "<title>" . $title . " - Municipio de Jerez</title>";
  ?>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta property="og:title" content="Municipio de Jerez">
  <meta property="og:type" content="P&aacute;gina de Gobierno del Municipio de Jerez">
  <meta property="og:url" content="http://jerez.gob.mx">
  <meta property="og:image" content="img/logo-wide.png">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="css/bulma.css">
  <link rel="stylesheet" href="css/all.css">
  <link rel="stylesheet" href="css/estilos.css">

  <meta name="theme-color" content="#fafafa">
</head>

<body>

    <div id="principalMenu"></div>

    <div class="py-1 has-background-main">
        <h1 class="py-1 is-title has-text-main has-text-centered has-text-weight-bold is-size-1">Comunicados Oficiales</h1>
    </div>

    <section class="section">
        <div class="container">
            <div class="columns is-multiline">
                <div class="column is-8">
                    <div class="card">
                        <div class="content p-3">
                            <h1 class="title has-text-weight-light is-2"><?php echo stripslashes($title);?></h1>
                            <time datetime="2016-1-1" class="has-text-right">Publicado el&nbsp;<?php echo $fecha;?></time>
                        </div>
                        <div class="card-image">
                            <figure id="frameImg" class="image is-3by2">
                                <img id="titleImg" class="img-modal img-to-zoom is-clickable" src="assets/news/img/<?php echo $foto1;?>"  alt="Imagen de Portada">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="content">
                                <?php echo stripslashes($note)?>
                                <div class="columns is-multiline">
                                    <div class="column is-6">
                                        <figure class="image is-3by2">
                                            <img class="img-modal img-to-zoom is-clickable" src="assets/news/img/<?php echo $foto1;?>" alt="Imagen de Contenido">
                                        </figure>
                                    </div>
                                    <div class="column is-6">
                                        <figure class="image is-3by2">
                                            <img class="img-modal img-to-zoom is-clickable" src="assets/news/img/<?php echo $foto2;?>" alt="Imagen de Contenido">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="card has-text-centered p-4">
                        <h1 class="has-text-weight-bold has-text-left pb-2">REDES SOCIALES</h1>
                        <div class="fb-page" data-href="https://www.facebook.com/gobjerez" data-tabs="timeline" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/gobjerez" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/gobjerez">H. Ayuntamiento de Jerez</a></blockquote></div>
                    </div>
                    <div class="my-4 p-6 has-background-stamp">
                        <div class="container columns">
                            <div class="column">
                                <h1 class="is-size-4 has-text-right has-text-main has-text-weight-bold pb-4">COMUNICADO OFICIAL</h1>
                                <p class="has-text-light has-text-right has-text-weight-light pb-2">Conforme a la ley de Protecci&oacute;n de Datos Personales en Poseci&oacute;n de Sujetos Obligados del Estado de Zacatecas.</p>
                                <p class="has-text-light has-text-right has-text-weight-light pb-2">El Gobierno Municipal de Jerez a tra&eacute;s de distintas areas, recaba amterial fotogr&aacute;fico y audiovisial para uso institucional en diferentes medios de comunicaci&oacute;n y redes sociales.</p>
                                <p class="has-text-light has-text-right has-text-weight-light">Si aparecees en alg&uacute;n material y prefieres que tu imagen sea retirada, asiste a nuestra Unidad de Transparencia (Benito Ju&aacute;rez #1, Centro, C.P. 99340) o cumun&iacute;cate al 494&nbsp;103&nbsp;4288.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section has-background-grey-lighter">
        <div class="container">
            <div class="has-text-centered">
                <img src="img/logo-wide.png" alt="Logo Jerez Transformando Jerez" style="height: 250px; width: auto;">
            </div>
        </div>
    </section>
    

    <script src="js/vendor/modernizr-3.11.2.min.js"></script>
    <script src="js/vendor/jquery-3.6.0.js"></script>
    <script src="js/vendor/all.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/menu.js"></script>
    <script src="js/nota/nota.js"></script>

    <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
    <script>
        window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
        ga('create', 'UA-XXXXX-Y', 'auto'); ga('set', 'anonymizeIp', true); ga('set', 'transport', 'beacon'); ga('send', 'pageview')
    </script>
    <script src="https://www.google-analytics.com/analytics.js" async></script>
</body>

</html>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v12.0&appId=617159405958164&autoLogAppEvents=1" nonce="Iy28HTal"></script>

